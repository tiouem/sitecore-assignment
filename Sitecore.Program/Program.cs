﻿using Sitecore;
using System;

namespace Sitecore.Program
{
    class Program
    {
        static void Main(string[] args)
        {
            var broker = new Broker.Broker();

            var newsPublisher = new Publisher.Publisher(broker, "News publisher");
            var weatherPublisher = new Publisher.Publisher(broker, "Weather publisher");


            var sub1 = new Subscriber.Subscriber("Subscriber 1 W & N");
            var sub2 = new Subscriber.Subscriber("Subscriber 1 W ");


            sub1.Subscibe(broker, Broker.BrokerUtils.MessageTopic.News);
            sub1.Subscibe(broker, Broker.BrokerUtils.MessageTopic.Weather);

            sub2.Subscibe(broker, Broker.BrokerUtils.MessageTopic.Weather);



            newsPublisher.PublishMessage(Broker.BrokerUtils.MessageTopic.News, new Broker.Message("AstraZeneca doesnt really work, looks like"));
            weatherPublisher.PublishMessage(Broker.BrokerUtils.MessageTopic.Weather, new Broker.Message("Today is 20 degrees in CPH"));

            Console.ReadKey();
        }
    }
}
