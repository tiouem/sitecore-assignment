using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Sitecore.Test
{
    [TestClass]
    public class SubscriberTest
    {
        [TestMethod]
        public void Subscriber_Can_Subscribe()
        {
            //A
            var brokerMock = new Mock<Broker.IBroker>();
            var subscriber = new Subscriber.Subscriber(It.IsAny<string>());
            var message = new Broker.Message(It.IsAny<string>());
            
            //A
            subscriber.Subscibe(brokerMock.Object, Broker.BrokerUtils.MessageTopic.Weather);

            //A
            brokerMock.Verify(x => x.OnSubscribe(Broker.BrokerUtils.MessageTopic.Weather, It.IsAny<Broker.Broker.EventHandler>()), Times.Once);

        }
    }
}
