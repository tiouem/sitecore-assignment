﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Sitecore.Broker.BrokerUtils;

namespace Sitecore.Test
{
    [TestClass]
    public class PublisherTest
    {
        [TestMethod]
        public void Publisher_Can_Publish()
        {
            //A
            var brokerMock = new Mock<Broker.IBroker>();
            var publisher = new Publisher.Publisher(brokerMock.Object, It.IsAny<string>());

            var message = new Broker.Message(It.IsAny<string>());
            //A

            publisher.PublishMessage(MessageTopic.Weather, message);

            //A
            brokerMock.Verify(x => x.Publish(MessageTopic.Weather, message), Times.Once);
        }
    }
}
