﻿using System;
using static Sitecore.Broker.BrokerUtils;

namespace Sitecore.Broker
{
    public class Broker : IBroker
    {

        public event EventHandler OnPromoteWeather;
        public event EventHandler OnPromoteNews;
        public event EventHandler OnPromoteSports;

        public void OnSubscribe(MessageTopic topic, EventHandler recievedMethod)
        {
            switch (topic)
            {
                case MessageTopic.Weather:
                    OnPromoteWeather += recievedMethod;
                    return;
                case MessageTopic.News:
                    OnPromoteNews += recievedMethod;
                    return;
                case MessageTopic.Sports:
                    OnPromoteSports += recievedMethod;
                    return;
                default: throw new ArgumentException($"Topic does not exist, {nameof(topic)}");
            }
        }

        public delegate void EventHandler(Broker broker, Message message);

        public void Publish(MessageTopic topic, Message message)
        {
            switch (topic)
            {
                case MessageTopic.Weather:
                    OnPromoteWeather?.Invoke(this, message);
                    return;
                case MessageTopic.News:
                    OnPromoteNews?.Invoke(this, message);
                    return;
                case MessageTopic.Sports:
                    OnPromoteSports?.Invoke(this, message);
                    return;
                default: throw new ArgumentException($"Topic does not exist, {nameof(topic)}");
            }
        }
    }
}
