﻿using static Sitecore.Broker.BrokerUtils;
using EventHandler = Sitecore.Broker.Broker.EventHandler;

namespace Sitecore.Broker
{
    public interface IBroker
    {
        void OnSubscribe(BrokerUtils.MessageTopic topic, EventHandler recievedMethod);
        void Publish(MessageTopic topic, Message message);
    }
}
