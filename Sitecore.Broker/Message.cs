﻿using System;

namespace Sitecore.Broker
{
    public class Message
    {
        public Message(string content)
        {
            Content = content;
            TimeStamp = DateTime.UtcNow;
        }

        public string Content { get; set; }
        public DateTime TimeStamp { get; init; }
    }
}
