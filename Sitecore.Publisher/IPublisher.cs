﻿using Sitecore.Broker;
using static Sitecore.Broker.BrokerUtils;

namespace Sitecore.Publisher
{
    public interface IPublisher
    {
        void PublishMessage(MessageTopic topic, Message message);
    }
}
