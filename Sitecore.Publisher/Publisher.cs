﻿using Sitecore.Broker;

namespace Sitecore.Publisher
{
    public class Publisher : IPublisher
    {
        private readonly IBroker _broker;

        public Publisher(IBroker broker, string publisherName)
        {
            _broker = broker;
            PublisherName = publisherName;
        }

        public string PublisherName { get; init; }

        public void PublishMessage(BrokerUtils.MessageTopic topic, Message message)
        {
            message.Content += $" Published by {PublisherName}";
            _broker.Publish(topic, message);
        }
    }
}
