﻿using Sitecore.Broker;
using static Sitecore.Broker.BrokerUtils;

namespace Sitecore.Subscriber
{
    public interface ISubscriber
    {
        public void Subscibe(IBroker broker, MessageTopic topic);
    }
}
