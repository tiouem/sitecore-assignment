﻿using Sitecore.Broker;
using System;

namespace Sitecore.Subscriber
{
    public class Subscriber : ISubscriber
    {
        public Subscriber(string sunscriberName)
        {
            SunscriberName = sunscriberName;
        }

        public string SunscriberName { get; init; }

        public void Subscibe(IBroker broker, BrokerUtils.MessageTopic topic)
        {
            broker.OnSubscribe(topic, OnMessageReceived);
        }

        protected virtual void OnMessageReceived(IBroker brooker, Message message)
        {
            Console.WriteLine($"Subscriber: {SunscriberName}.  ||  Message : {message.Content}, created at: {message.TimeStamp}, recieved at: {DateTime.UtcNow}");
        }
    }
}
